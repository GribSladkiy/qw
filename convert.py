from functools import reduce

import bs4.element


class TagToText:
    """Преобразователь тагов к тексту"""

    def __init__(self, rules: dict, list_of_tag: list[bs4.element.Tag], source: str, text_tag: list):
        self.rules = rules
        self.list_of_tag = list_of_tag
        self.text_output = ''
        self.text_tag = text_tag
        self.source = source

    def process(self) -> str:
        """Преобразование тегов в текст"""
        output = ''
        if not self.text_output:
            output = reduce(lambda acc, x: acc + x, map(self.wrapper, self.list_of_tag))
        self.text_output = output

        return self.text_output

    def wrapper(self, tag: bs4.element.Tag) -> str:
        """Упаковка тегов в текст согласно правилам, рекурсивный проход по тегам и их упаковка,
        рекурсия останавливается, если элемент является строкой"""
        result = ''
        if isinstance(tag, bs4.element.NavigableString):
            return tag.strip()
        if tag.name in self.rules:
            result += self.rules[tag.name](tag, self.source)
            if tag.name in self.text_tag:
                for children in tag.contents:
                    result += self.wrapper(children)
                result += self.rules[tag.name](tag, self.source)
        return result
