import re
import textwrap


class TextFormatter:
    """Форматирует текст к указанной ширине"""

    def __init__(self, input_text: str, max_len: int):
        self.processed_text = ''
        self.input_text = input_text
        self.text_wrapper = textwrap.TextWrapper(width=max_len, tabsize=4)
        self.delete_extra_line()

    def format_text(self) -> str:
        """Форматирование текста"""
        raw_text = '\n'.join(
            '\n'.join(
                self.text_wrapper.wrap(x)) for x in self.input_text.splitlines())
        processed_text = re.sub(' ', ' ', raw_text, flags=re.IGNORECASE)
        processed_text = processed_text.removeprefix('\n')
        return processed_text

    def delete_extra_line(self) -> None:
        """Удаление повторяющихся переносов строки"""
        output = ''
        for ind, line in enumerate(self.input_text.splitlines()):
            if ind > 0:
                if line == '' and self.input_text.splitlines()[ind - 1] == '':
                    continue
            output += (line + '\n')
        self.input_text = output
