from urllib.parse import urlparse
import os


class UrlToFileSystemPath:
    """Преобразует url в путь в файловой системе"""
    def __init__(self, url: str):
        self.url = url

    def build_file_system_path(self) -> str:
        url = urlparse(self.url)
        netloc = url.netloc
        if netloc.count('.') > 1:
            netloc = '.'.join(netloc.split('.')[1:])
        return os.path.join(netloc, url.path.removeprefix('/'))
