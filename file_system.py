import os


class FileSystemWorker:
    """Обработчик взаимодействия с файловой ситемой"""
    def __init__(self, file_path):
        self.file_path = file_path
        self.dir = ''
        self.file_name = ''
        self.default_file_name = 'index'

    def split_file_path(self):
        """Разделение пути на директорию и сам файл, если имя файла пустое, то заменяет его на дефолтное"""
        self.dir = os.path.dirname(self.file_path)
        self.file_name = os.path.basename(self.file_path)
        if not self.file_name:
            self.file_name = self.default_file_name

    def add_type_to_file(self, file_type):
        """Добавление требуемого типа к имени файла"""
        if self.file_name.count('.'):
            self.file_name = self.file_name.rsplit('.', maxsplit=1)[0]
        self.file_name += file_type

    def create_directory(self):
        """Создание директории"""
        if not os.path.exists(self.dir):
            os.makedirs(self.dir)

    def preprocess_file_system(self, file_type):
        """Подготовка файловой системы"""
        self.split_file_path()
        self.add_type_to_file(file_type)
        self.create_directory()

    def get_path(self):
        """Возвращение пути файла"""
        return os.path.join(self.dir, self.file_name)
