import builtins

from abc import abstractmethod


class FileSystemWriter:
    """Обработчик для записи в файловую систему"""

    def __init__(self, file_path: str, content: str, file_system_worker: builtins):
        self.content = content
        self.file_type = ''
        self.file_system_handler = file_system_worker(file_path)

    @abstractmethod
    def save(self):
        """Абстрактная функция, реализующая сохранение"""
        pass

    def preprocess_filesystem(self):
        """Предобработка файловой системы"""
        self.file_system_handler.preprocess_file_system(self.file_type)

    @abstractmethod
    def preprocess_content(self):
        """Предобработка контентка"""
        pass

    def get_path(self) -> str:
        """Возвращает путь, по которому необходимо сохранить файлы"""
        return self.file_system_handler.get_path()


class TextWriter(FileSystemWriter):
    def __init__(self, file_path: str, content: str, file_system_worker: builtins):
        super().__init__(file_path, content, file_system_worker)
        self.file_type = '.txt'

    def save(self):
        """Реализация метода сохранения"""
        self.preprocess_filesystem()
        with open(self.get_path(), 'w') as f:
            f.write(self.content)

    def preprocess_content(self):
        """Предобработка контентка"""
        pass
