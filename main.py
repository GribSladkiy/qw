from urllib.parse import urlparse
import sys
import file_system
import content_extractor
import path_builder
import text_handler
import writer
import convert
import requests
import settings

if __name__ == '__main__':
    url = ''
    resp = None
    try:
        url = sys.argv[1]
        resp = requests.get(url)
        if resp.status_code == 200:
            source = '://'.join([urlparse(url).scheme, urlparse(url).netloc])
            max_len, content_name, main_container, containers_tag, text_tag, format_rules = settings.get_rules()

            extractor = content_extractor.PageTextExtractor(page_content=resp.text, container_tag=containers_tag,
                                                            text_tag=text_tag,
                                                            main_container=main_container, content_name=content_name)

            converter = convert.TagToText(
                list_of_tag=extractor.extract(),
                rules=format_rules,
                source=source, text_tag=text_tag)

            text_formater = text_handler.TextFormatter(input_text=converter.process(), max_len=max_len)

            path = path_builder.UrlToFileSystemPath(url=url).build_file_system_path()

            text_writer = writer.TextWriter(file_path=path, content=text_formater.format_text(),
                                            file_system_worker=file_system.FileSystemWorker)
            text_writer.save()
    except Exception as e:
        pass
