import format_rules


max_len = 80
content_name = 'content'
main_container = 'article'
containers_tag = ['div', 'section', 'main', 'article']
text_tag = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'h8', 'header']

format_rules = {
    'a': format_rules.replace_href,
    'p': format_rules.add_line,
    'h1': format_rules.add_line,
    'h2': format_rules.add_line,
    'h3': format_rules.add_line,
    'h4': format_rules.add_line,
    'h5': format_rules.add_line,
    'h6': format_rules.add_line,
    'h7': format_rules.add_line,
    'h8': format_rules.add_line,
    'header': format_rules.add_line,
}


def get_rules():
    return max_len, content_name, main_container, containers_tag, text_tag, format_rules
