from functools import reduce
from typing import Iterator
import bs4.element
from bs4 import BeautifulSoup


class PageTextExtractor:
    def __init__(self, page_content: str, main_container: str, container_tag: list, text_tag: list, content_name: str):
        self.soup_body_content = BeautifulSoup(page_content, 'lxml').find('body')
        self.main_container = main_container
        self.content_name = content_name
        self.containers_tag = container_tag
        self.text_tag = text_tag
        self.output = []

    def search_content(self, tag: bs4.element.Tag):
        if tag.attrs.get('id', None):
            if self.content_name in tag.attrs.get('id').lower():
                return True
        if tag.attrs.get('class', None):
            if self.content_name in [i.lower() for i in tag.attrs.get('class')]:
                return True
        return False

    def get_page_content_iterator(self) -> Iterator:
        # возвращает итератор на элементы с основным контейнером
        content_iterator = self.soup_body_content.find_all(self.main_container)
        # если на страничке нет элементов с типом основного контейнера используется метод по поиску контента
        if not len(list(content_iterator)):
            content_iterator = self.soup_body_content.find_all(self.search_content, recursive=False)
        return content_iterator

    def extract(self) -> list[bs4.element.Tag]:
        """Извлечение статьи со странички.
           Находим все потенциальные элементы со статьей, находим все контейнеры и достаем оттуда элементы,
           содержащие текст, т.к таких элементов может быть несколько, выбираем самуй обьемный
           (определяется по абсолютному количеству символов )"""
        current_article_content = []
        max_len = 0
        for article in self.get_page_content_iterator():

            if not self.is_navigation_link(article):
                # получаем текстовые элементы из контейнера
                for container in article.find_all(self.containers_tag):
                    container_content = self.get_main_content(container)

                    if len(container_content):
                        current_article_content.append(container_content)

                current_article_content = self.make_single_dimension_list(current_article_content)
                current_article_size = PageTextExtractor.get_content_size(current_article_content)
                if max_len < current_article_size:
                    max_len = current_article_size
                    self.output = current_article_content
        return self.output

    @staticmethod
    def get_content_size(current_article_content):
        """Возвращает абсолютную длину статьи"""
        if not len(current_article_content):
            return 0
        return len(list(reduce(lambda acc, x: acc + x, map(lambda x: x.text, current_article_content))))

    def make_single_dimension_list(self, element_or_list_of_elements) -> list:
        """Приведение списка с элементами к 1 размерности"""
        result = []
        for obj in element_or_list_of_elements:
            if isinstance(obj, list):
                result.extend(self.make_single_dimension_list(obj))
            else:
                result.append(obj)
        return result

    @staticmethod
    def is_navigation_link(parent: bs4.element.Tag) -> bool:
        """Проверяет является ли весь текст элемента ссылкой"""
        tags_child = list(filter(lambda x: isinstance(x, bs4.element.Tag), parent.contents))
        if len(tags_child) == 1:
            # Если родительский элемент имеет только один дочерний тэг - с типом a, весь текст которого является ссылкой
            child = tags_child[0]
            if child.name == 'a':
                if parent.a.text == child.text:
                    return True
            return False
        return False

    def get_main_content(self, tag: bs4.element.Tag) -> list:
        """
        Если элемент содержит текст, то итерируемся по дочерним,
        если дочерний - "текстосодержащий", то добавляем его текст к результату
        """
        result = list()
        if tag.text:
            for children in tag.contents:
                if children.name in self.text_tag:
                    if not PageTextExtractor.is_navigation_link(children):
                        result.append(children)
            return result
        else:
            return []
