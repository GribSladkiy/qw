from urllib.parse import urlparse, urljoin

import bs4


def add_line(_, __):
    return '\n'


def is_absolute(url):
    return bool(urlparse(url).netloc)


def replace_href(soup_obj: bs4.element.Tag, link):
    href = soup_obj.get('href')
    if not is_absolute(href):
        href = urljoin(link, href)
    return '[' + href + ']' + ' ' + soup_obj.text + ' '
